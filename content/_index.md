## Front Page Content

Bienvenue sur mon site qui deviendra stylé grâce à [Hugo](https://gohugo.io).
Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. 
Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
